package asmt.pkg4.pkg1;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

public class Asmt41 {

    public static void main(String[] args) {
        //create a printstream called fileOut to write to the output text file
        PrintStream fileOut = null;
        //create a scanner to read input from the user
        Scanner input = new Scanner(System.in);

        //ask the user for which file they want to read from
        System.out.println("What file do you want to read from?");
        //Tell the user not to put .txt because the program automatically does it 
        //If you do it will end up (the file name).txt.txt
        System.out.println("You do not need to put the .txt for the file name");
        //get whatever the user put(It's suppose to be the name of the file)
        String fileName = input.nextLine();
        //close the scanner
        input.close();

        //use try catch to make the input text file scanner
        try {
            //print that it's making the scannner to the console
            System.out.println("Making the scanner for reading the text file");
            //actually make the scanner for the text file
            input = new Scanner(new File(fileName + ".txt"));
            //catch any expection
        } catch (Exception e) {
            //print to the console that there was a problem using the error stream
            System.err.println("There was a problem creating the file scanner");
            //end the program
            System.exit(1);
        }

        //use try catch to make the printstream
        try {
            //print to the console that it's making the printstream
            System.out.println("Making the print stream");
            //actually make the printstream
            fileOut = new PrintStream(new File(fileName + "_out.txt"));
            //catch any expection
        } catch (Exception e) {
            //print to the console there was a problem
            System.err.println("There was a problem creating the print stream for the output file");
            //end the program
            System.exit(1);
        }

        //loop while the scanner has another line that it can read
        while (input.hasNextLine()) {

            //variable called lineIn to hold the value of the line from the text file
            String lineIn = input.nextLine();
            //lineIn = lineIn.trim();

            //We check every index of Mike then check if it meets our criteria 
            //if it meets the criteria we replace it with the corsponding Michael
            int index = lineIn.indexOf("Mike");
            //variable called prevIndex to hold what the last index was for getting the parts that are not mike
            int prevIndex = 0;
            //string called out for storinng the output 
            //with a inital value of empty string to avoid an issue with the variable hasn't been initalized
            String out = "";

            //line is just "Mike"
            if (lineIn.equals("Mike")) {
                out = "Michael";
                index = -1;
                prevIndex = lineIn.length() - 1;
                //Line is just "Mike's"
            } else if (lineIn.equals("Mike's")) {
                out = "Michael's";
                index = -1;
                prevIndex = lineIn.length() - 1;
                //Line is just "Mike,"
            } else if (lineIn.equals("Mike,")) {
                out = "Michael,";
                index = -1;
                prevIndex = lineIn.length() - 1;
                //Line is just "Mike's,"
            } else if (lineIn.equals("Mike's,")) {
                out = "Michael's,";
                index = -1;
                prevIndex = lineIn.length() - 1;
            }

            //starts with "Mike "
            if (lineIn.startsWith("Mike ")) {
                //set out to Michael
                out = "Michael ";
                //set the previous index to 5 the lenght of "Mike " + 1
                prevIndex = 5;
                //find next occurance of Mike
                index = lineIn.indexOf("Mike", index + 1);
                //starts with "Mike's "
            } else if (lineIn.startsWith("Mike's ")) {
                //set out to "Michael's "
                out = "Michael's ";
                //set the previous index to 7 the length of Mike's + 1
                prevIndex = 7;
                //find the next occurance of mike
                index = lineIn.indexOf("Mike", index + 1);
                //Line starts with "Mike, "
            } else if (lineIn.startsWith("Mike, ")) {
                //make out start with "Michael, "
                out = "Michael, ";
                //set the previous index to 6(the lenght of "Mike, "+1)
                prevIndex = 6;
                //find the next occurance of mike
                index = lineIn.indexOf("Mike", index + 1);
                //Line starts with "Mike's "
            } else if (lineIn.startsWith("Mike's, ")) {
                //set out "Michael's, "
                out = "Michael's, ";
                //set the previous index to 8(the length of "Mike,s "+1)
                prevIndex = 8;
                //find the next occurance of mike
                index = lineIn.indexOf("Mike", index + 1);
            }
            //loop while the index of mike is greater than or equal to 0
            while (index >= 0) {
                //checking to see if we will not go out of bounds with check for if the mike we are looking at is " Mike "
                if (index - 1 >= 0 && index + 5 <= lineIn.length() - 1 && lineIn.substring(index - 1, index + 5).equals(" Mike ")) {
                    //add what ever was between the last mike we checked and the current mike with the appropiate Michael
                    out = out + lineIn.substring(prevIndex, index) + lineIn.substring(prevIndex, index - 1) + " Michael ";
                    //set the previous index to be 5 past the current index
                    prevIndex = index + 5;
                    //checking if we will not go out of bounds and the mike we are looking at is " Mike's "

                } else if (index - 1 >= 0 && index + 7 <= lineIn.length() - 1 && lineIn.substring(index - 1, index + 7).equals(" Mike's ")) {
                    //add anything from the previous index to the current index-1 with " Michael's "
                    out = out + lineIn.substring(prevIndex, index - 1) + " Michael's ";
                    //set the previous index to the current index + 7
                    prevIndex = index + 7;
                    //check if we will not go out of bounds and the mike we are looking at is " Mike's, "

                } else if (index - 1 >= 0 && index + 8 <= lineIn.length() - 1 && lineIn.substring(index - 1, index + 8).equals(" Mike's, ")) {
                    //add anything from the previous index to the current index-1 with " Michael's, "
                    out = out + lineIn.substring(prevIndex, index - 1) + " Michael's, ";
                    //set the previous index to the current index + 8
                    prevIndex = index + 8;
                    //checking to see if we will go out of bounds and the mike we are looking at is " Mike, "

                } else if (index - 1 >= 0 && index + 6 <= lineIn.length() - 1 && lineIn.substring(index - 1, index + 6).equals(" Mike, ")) {
                    //add anything from the previous index to the current index-1 + " Michael, "
                    out = out + lineIn.substring(prevIndex, index - 1) + " Michael, ";
                    //set the previous index to the current index + 6
                    prevIndex = index + 6;
                }

                //find the next occurance of Mike and repeat the process
                index = lineIn.indexOf("Mike", index + 1);
            }
            //ends with " Mike"
            if (lineIn.endsWith(" Mike")) {
                //add " Michael" to the end
                out = out + lineIn.substring(prevIndex, lineIn.lastIndexOf("Mike") - 1) + " Michael";
                //make the previous index the length of the line
                prevIndex = lineIn.length() - 1;
                //ends with " Mike's"
            } else if (lineIn.endsWith(" Mike's")) {
                //add " Michael's" to the end
                out = out + lineIn.substring(prevIndex, lineIn.lastIndexOf("Mike") - 1) + " Michael's";
                //set the previous index to the length of the line
                prevIndex = lineIn.length() - 1;
                //ends with " Mike's,"
            } else if (lineIn.endsWith(" Mike's,")) {
                //add " Michael's," to the end
                out = out + lineIn.substring(prevIndex, lineIn.lastIndexOf("Mike") - 1) + " Michael's,";
                //set the previous index to the length of the line
                prevIndex = lineIn.length() - 1;
                //ends with " Mike,"
            } else if (lineIn.endsWith(" Mike,")) {
                //add " Michael," to the end
                out = out + lineIn.substring(prevIndex, lineIn.lastIndexOf("Mike") - 1) + " Michael,";
                //set the previous to the length of the line
                prevIndex = lineIn.length() - 1;
            }

            //still characters remaining that we haven't done anything with yet
            if (prevIndex < lineIn.length() - 1) {
                //add anything that is after the last mike that we changed add it to the end of out
                out = out + lineIn.substring(prevIndex);
            }

            //display what ever the value of out is on the console 
            //primarily for debugging
            System.out.println(out);
            //write the value of out in the output text file
            fileOut.println(out);
        }

        //close the input file scanner
        input.close();
        //close the output file writer
        fileOut.close();

    }

}
